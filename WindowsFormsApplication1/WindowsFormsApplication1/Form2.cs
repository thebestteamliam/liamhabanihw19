﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form2 : Form
    {
        private string username;
        public Form2()
        {
            InitializeComponent();
        }
        public Form2(string username)
        {
            this.username = username;
            InitializeComponent();
        }
        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            string[] userDate;
            string date = monthCalendar1.SelectionRange.Start.ToShortDateString();
            StreamReader reader = new StreamReader(username +"BD.txt");
            bool good = false;
            do
            {
                userDate = reader.ReadLine().Split(',');
                if(userDate[1]==date)
                {
                    good = true;
                }

            }
            while (reader.Peek() != -1 && !good);
            if(good)
            {
                label1.Text = "חוגג יום הולדת " + userDate[0] + "בתאריך הנבחר - ";
            }
            else
            {
                label1.Text = "בתאריך הנבחר אף אחד לא חוגג יום הולדת";
            }
        }
    }
}
