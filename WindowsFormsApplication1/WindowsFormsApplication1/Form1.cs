﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        Form form2;
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string []userpass;
            StreamReader reader = new StreamReader("Users.txt");
            bool good = false;
            do
            {
                string line = reader.ReadLine();
                userpass =line.Split(',');
                if(userTB.Text == userpass[0] && passTB.Text ==userpass[1])
                {
                    good =true;
                }
            }   
            while(reader.Peek() != -1 && !good);
            if(good)
            {
                form2 = new Form2(userpass[0]);
                this.Hide();
                form2.ShowDialog();
            }
            else
            {
                MessageBox.Show("שם משתמש או סיסמה לא נכונים");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

    }
}
